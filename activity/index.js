/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function myBioDetails() {
		let fullName = prompt("Your Full Name");
		let age = prompt("Your Age");
		let location = prompt("Your Location");

		console.log("Hello, I'm " + fullName);
		console.log("I'm " + age + " years old.");
		console.log("I lived in " + location + " City");
	
	}
	myBioDetails();
	
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function myFavoriteBands() {
		let band1 = "ERASERHEADS";
		let band2 = "RIVERMAYA";
		let band3 = "PAROKYA NI EDGAR";
		let band4 = "THE DAWN";
		let band5 = "SIAKOL";

		console.log("1. " + band1);
		console.log("2. " + band2);
		console.log("3. " + band3);
		console.log("4. " + band4);
		console.log("5. " + band5);
	}
	myFavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function myFavoriteMovies() {
		let movie1 = "JOHN WICK I, II, III, IV";
		let movieRating1 = "Rotten Tomatoes Rating: 99%";
		let movie2 = "THE EXPENDABLES I, II , III, IV";
		let movieRating2 = "Rotten Tomatoes Rating: 96%";
		let movie3 = "EXTRACTION (2020)";
		let movieRating3 = "Rotten Tomatoes Rating: 67%";
		let movie4 = "FAST & FURIOUS";
		let movieRating4 = "Rotten Tomatoes Rating: 59%";
		let movie5 = "GUARDIANS OF THE GALAXY";
		let movieRating5 = "Rotten Tomatoes Rating: 81%";

		console.log("1. " + movie1);
		console.log(movieRating1);
		console.log("2. " + movie2);
		console.log(movieRating2);
		console.log("3. " + movie3);
		console.log(movieRating3);
		console.log("4. " + movie4);
		console.log(movieRating4);
		console.log("5. " + movie5);
		console.log(movieRating5);
	}
	myFavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();
// console.log(friend1);
// console.log(friend2);